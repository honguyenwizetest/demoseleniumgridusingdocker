import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class ThirdTest extends BaseTest{

    @Test
    public void MyTest3_1() throws Exception {
        Reporter.log(String.format("MyTest3_1 Test Started %s",Thread.currentThread().getId()),true);
        getDriver().navigate().to("https://www.google.com/");
        Thread.sleep(3000);
        Reporter.log(String.format("MyTest3_1 Test Ended %s",Thread.currentThread().getId()),true);
    }

    @Test
    public void MyTest3_2() throws Exception {
        Reporter.log(String.format("MyTest3_2 Test Started %s",Thread.currentThread().getId()),true);
        getDriver().navigate().to("https://www.google.com/");
        Thread.sleep(3000);
        Reporter.log(String.format("MyTest3_2 Test Ended %s",Thread.currentThread().getId()),true);
    }

    @Test
    public void MyTest3_3() throws Exception {
        Reporter.log(String.format("MyTest3_3 Test Started %s",Thread.currentThread().getId()),true);
        getDriver().navigate().to("https://www.google.com/");
        Thread.sleep(3000);
        Reporter.log(String.format("MyTest3_3 Test Ended %s",Thread.currentThread().getId()),true);
    }

    @Test
    public void MyTest3_4() throws Exception {
        Reporter.log(String.format("MyTest3_4 Test Started %s",Thread.currentThread().getId()),true);
        getDriver().navigate().to("https://www.google.com/");
        Thread.sleep(3000);
        Reporter.log(String.format("MyTest3_4 Test Ended %s",Thread.currentThread().getId()),true);
    }


}